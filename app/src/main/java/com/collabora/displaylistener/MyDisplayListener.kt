// Copyright 2020, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
// Author: Ryan Pavlik <ryan.pavlik@collabora.com>


package com.collabora.displaylistener

import android.app.Presentation
import android.content.Context
import android.content.ContextWrapper
import android.hardware.display.DisplayManager
import android.util.Log
import androidx.fragment.app.FragmentManager

class MyDisplayListener(base: Context, private val fragmentManager: FragmentManager) : DisplayManager.DisplayListener, ContextWrapper(base) {
    companion object {
        const val TAG = "MyDisplayListener"
    }

    /**
     * Called whenever a logical display has been added to the system.
     * Use [DisplayManager.getDisplay] to get more information about
     * the display.
     *
     * @param displayId The id of the logical display that was added.
     */
    override fun onDisplayAdded(displayId: Int) {
        Log.i(TAG, "onDisplayAdded($displayId)")
        val displayManager = getSystemService(DisplayManager::class.java)
        val display = displayManager.getDisplay(displayId)
        val displayContext = createDisplayContext(display)
        val pres = Presentation(displayContext, display)
        val fragment = fragmentManager.fragmentFactory.instantiate(classLoader, FullscreenFragment::class.qualifiedName!!)
        val fullscreenFragment = fragment as FullscreenFragment

        fullscreenFragment.show(baseContext, display, fragmentManager)
    }

    /**
     * Called whenever a logical display has been removed from the system.
     *
     * @param displayId The id of the logical display that was removed.
     */
    override fun onDisplayRemoved(displayId: Int) {
        Log.i(TAG, "onDisplayRemoved($displayId)")
    }

    /**
     * Called whenever the properties of a logical [android.view.Display],
     * such as size and density, have changed.
     *
     * @param displayId The id of the logical display that changed.
     */
    override fun onDisplayChanged(displayId: Int) {
        Log.i(TAG, "onDisplayChanged($displayId)")
    }
}